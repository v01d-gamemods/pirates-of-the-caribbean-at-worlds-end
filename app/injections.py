import trainerbase.codeinjection
import trainerbase.memory


infinite_hp = trainerbase.codeinjection.CodeInjection(
    trainerbase.memory.pm.base_address + 0x55C2, b"\x90" * 6
)

infinite_items = trainerbase.codeinjection.CodeInjection(
    trainerbase.memory.pm.base_address + 0x79666, b"\x90" * 3
)
